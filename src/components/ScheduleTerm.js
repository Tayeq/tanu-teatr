import React from 'react';

const ScheduleTerm = ({term:{date, description}}) => (
        <div className="schedule-term">
            <h1 className="schedule-term__date">{date}</h1>
            <p className="schedule-term__description">{description}</p>
        </div>
    );

export default ScheduleTerm;