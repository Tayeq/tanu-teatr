import React from 'react';
import {Container, Row, Col} from "reactstrap";

const Footer = () => {
    return (
        <footer>
            <Container>
                <div className="contact">
                    <h1>Kontakt</h1>
                    <p>
                        Teatr Zagłębia, ul. Teatralna 4, 41-200 Sosnowiec<br/>
                        Agata Kędzia (+48) 883 500 915<br/>
                        Filip Jałowiecki (+48) 692 496 819
                    </p>
                </div>
                <div className="patreons">
                    <h1>Patronat</h1>
                    <Row>
                        <Col md="3">
                            <h2>Patronat honorowy:<br/>Prezydent miasta Sosnowca</h2>
                            <img className="img-fluid" src="./assets/patreons/Sosnowiec.png"/>
                        </Col>
                        <Col md="6" className="px-5 patreons__medial">
                            <h2>Patrnerzy medialni</h2>
                            <Row>
                                <Col md="3" xs="6"><img className="img-fluid" src="./assets/patreons/e-teatr.png"/></Col>
                                <Col md="3" xs="6"><img className="img-fluid" src="./assets/patreons/miejski_kurier.png"/></Col>
                                <Col md="3" xs="6"><img className="img-fluid" src="./assets/patreons/szum.png"/></Col>
                                <Col md="3" xs="6"><img className="img-fluid" src="./assets/patreons/teatralny.png"/></Col>
                            </Row>
                            <Row className="mt-20">
                                <Col md="3" xs="6"><img className="img-fluid" src="./assets/patreons/dziennik_teatralny.png"/></Col>
                                <Col md="3" xs="6"><img className="img-fluid" src="./assets/patreons/wyborcza.png"/></Col>
                                <Col md="3" xs="6"><img className="img-fluid" src="./assets/patreons/tvp3.png"/></Col>
                            </Row>
                        </Col>
                        <Col md="3">
                            <img className="img-fluid last-img" src="./assets/patreons/ministerstwo.png"/>
                        </Col>
                    </Row>
                </div>
            </Container>
        </footer>
    );
};

export default Footer;
