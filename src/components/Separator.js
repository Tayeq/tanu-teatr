import React from 'react';

const Separator = () => {
    return (
        <img className="separator animate__animated animate__fadeIn" src="./assets/images/separator.png"/>
    );
};

export default Separator;