import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import {Fade, Slide} from 'react-reveal';
import CompetitionWork from "./CompetitionWork";

const works = [
    {
        url: 'https://youtu.be/lSKGmfBLhUI',
        title: 'Sanatorium',
        author: 'Barbara Gryka',
        cast: 'Występują: Joanna Połeć, Aleksander Blitek, Mirosława Żak' + "\n" +
            'Performerzy: Barbara Gryka, Filip Kijowski (współpraca performance)'+ "\n" +
            'Montaż: Karol Jakóbczyk' + "\n" +
            'Dźwięk: Jacek Sotomski',
        description: 'W swoim projekcie zastanawiam się czy nasza komunikacja będzie musiała przejść drastyczną zmianę w związku z sytuacją w której się znaleźliśmy. Czy tęsknota za spotkaniem z drugim człowiekiem mogła by przełożyć się na pozytywna zmianę w nas samych. Czy wychodząc ze swojego pancerza bezpieczeństwa moglibyśmy stać się bardziej tolerancyjni i otwarci na drugą osobę? ',
        parts: []
    },
    {
        url: 'https://youtu.be/lSKGmfBLhUI',
        title: 'Agartha',
        author: 'Adrianna Alksnin',
        cast: 'Występują: Małgorzata Saniak-Grabowska, Agnieszka Okońska-Bałaga, Michał Bałaga, Kamila Janik, Konrad Stein' + "\n" +
        'Scenariusz i reżyseria: Adrianna Alksnin ' + "\n" +
        'Scenografia i kostiumy: Katarzyna Sobolewska' + "\n" +
        'Muzyka: Filip Franczak' + "\n" +
        'Montaż dźwięku: Jacek Sotomski',
        description: 'W obliczu zbliżającej się katastrofy klimatycznej, grupa śmiałków podejmuje próbę dotarcia do Agharty — mitycznej, podziemnej krainy, zamieszkiwanej przez zaawansowaną cywilizację. Czy uda im się odnaleźć wejście do wnętrza Ziemi? Czy uchronią planetę przed zagładą? ',
        parts: [
            {url: 'https://youtu.be/lSKGmfBLhUI', title: 'część 2'},
            {url: 'https://youtu.be/lSKGmfBLhUI', title: 'część 3'},
            {url: 'https://youtu.be/lSKGmfBLhUI', title: 'część 4'},
            {url: 'https://youtu.be/lSKGmfBLhUI', title: 'część 5'},
        ]
    },
];

const CompetitionWorks = () => {
    return (
        <section className="competition-works">
            <Container>
                <Fade>
                    <h1 className="section-title">Prace zwycięskie</h1>
                </Fade>

                <div className="competition-works__content pt-5">
                    <Row className="mb-5">
                        <Col md="4">
                            <p className="font-bold">
                                Zwyciężczynie konkursu przyglądają się przyszłości z dwóch różnych perspektyw – miniserial Adrianny Alksnin fantazyjnie dialoguje z kinem gatunkowym, natomiast Barbara Gryka w swoim projekcie wideo zwraca się ku rzeczywistości naznaczonej światową pandemią. W ramach grantu zwyciężczynie konkursu rozwijają początkowe wersje swoich pomysłów, efekty ich prac będą dostępne po 20 września 2020.
                            </p>
                        </Col>
                        <Col md="8" >
                            <img className="img-fluid" src="./assets/images/competition_works.png"/>
                        </Col>
                    </Row>
                    {works.map(work => <CompetitionWork work={work} key={work.toString()}/>)}
                </div>
            </Container>
        </section>
    );
};

export default CompetitionWorks;
