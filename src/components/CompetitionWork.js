import React from 'react';
import {Row, Col} from 'reactstrap';
import ReactPlayer from 'react-player'

const CompetitionWork = ({work:{url, title, author, cast, description, parts}}) => (
    <div className="competition-work">
        <Row>
            <Col md="1" className="d-md-block d-none p-0 pt-5">
                <img className="competition-work__leftImage img-fluid" src="./assets/images/competition_work_left.png" alt="Competition Work"/>
            </Col>
            <Col md="7">
                <ReactPlayer url={url} className="embed-responsive embed-responsive-16by9" width="100%" height="auto"/>
                <div className="competition-work__parts my-3">
                    {parts.map((part) =>
                        <a className="competition-work__part" href={part.url} target="_blank">
                            {part.title}
                        </a>
                    )}
                </div>
            </Col>
            <Col md="4">
                <div className="competition-work__content">
                    <h1 className="competition-work__title">{title}</h1>
                    <h2 className="competition-work__author">{author}</h2>
                    <p className="competition-work__cast font-bold">
                        {cast.split('\n').map((item, i) => <p key={i}>{item}</p>)}
                    </p>

                    <p className="competition-work__description">{description}</p>
                </div>
            </Col>

        </Row>
    </div>


    );

export default CompetitionWork;
