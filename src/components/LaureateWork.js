import React from 'react';

const LaureateWork = ({work:{title, authors, links}}) => (
        <div className="laureate-work">
            <div className="laureate-work__content">
                <div className="laureate-work__title">
                    {title}
                </div>
                <div className="laureate-work__authors">
                    {authors.map((author) =>
                        <p className="laureate-work__author">
                            {author}
                        </p>
                    )}
                </div>
                <div className="laureate-work__links">
                    {links.map((link) =>
                        <a className="laureate-work__link" href={link.url} target="_blank">
                            {link.title}
                        </a>
                    )}
                </div>
            </div>
        </div>
    );

export default LaureateWork;
