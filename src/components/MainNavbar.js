import React, {useEffect, useState} from 'react'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    NavbarText,
    Container
} from 'reactstrap';
import {scroller, animateScroll} from 'react-scroll/modules'

const scrollOptions = {
    duration: 300,
    delay: 15,
    smooth: 'easeInOutQuart',
    offset: -104,
}

const MainNavbar = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [isScrolled, setIsScrolled] = useState(false);
    const [isSmall, setIsSmall] = useState(false)

    const toggle = () => setIsOpen(!isOpen);
    const catchScroll = () => {
        const position = window.scrollY
        setIsScrolled(!!position)
        setIsSmall(!!position)
    }
    const _closeMenu = () => setIsOpen(false)
    const scrollToSection = name => {
        scroller.scrollTo(name, scrollOptions)
        _closeMenu()
    }

    useEffect(() => {
        setIsSmall(!!window.scrollY)
        window.addEventListener('scroll', catchScroll)
        return () => {
            window.removeEventListener('scroll', catchScroll)
        }
    }, []);

    return (

            <Navbar expand="md" light id="main-navbar"
                    className={`${(isOpen || isScrolled) && '-bg-yellow'} ${isScrolled && '-small'}`}>
                <Container>
                <button className="navbar-brand navbar__btn" onClick={animateScroll.scrollToTop}><img
                    className={`navbar__logo ${!isOpen && '-small'}`}
                    src="./assets/images/logo.png" alt="logo"/></button>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="" navbar>
                        <NavItem>
                            <button type="button" className="navbar__btn"
                                    onClick={() => {
                                        scrollToSection('description')
                                    }}>opis konkursu
                            </button>
                        </NavItem>
                        <NavItem>
                            <button type="button" className="navbar__btn" onClick={() => {
                                scrollToSection('competition-works')
                            }}>prace konkursowe
                            </button>
                        </NavItem>
                        <NavItem>
                            <button type="button" className="navbar__btn" onClick={() => {
                                scrollToSection('schedule')
                            }}>harmonogram
                            </button>
                        </NavItem>
                        <NavItem>
                            <button type="button" className="navbar__btn" onClick={() => {
                                scrollToSection('contact')
                            }}>kontakt
                            </button>
                        </NavItem>
                        <NavItem className="navbar__arrowTop d-md-none text-center">
                            <button type="button" className="navbar__btn" onClick={() => {
                                toggle()
                            }}><img src="./assets/icons/arrow_top.png" alt="Arrow top"/>
                            </button>
                        </NavItem>
                    </Nav>
                </Collapse>
                <NavbarText className="navbar__theatre-logo">
                    <a href="https://teatrzaglebia.pl/" target="_blank" >
                        <img className="navbar__theatre-logo_img mr-4" src="./assets/images/teatr-logo.png" alt="logo"/>
                    </a>
                </NavbarText>
                </Container>
            </Navbar>


    )
}

export default MainNavbar
