import React, {useState} from 'react';
import {NavbarText, NavItem} from "reactstrap";
import {scroller, animateScroll} from 'react-scroll/modules'
import App from "../App";

const scrollOptions = {
    duration: 300,
    delay: 15,
    smooth: 'easeInOutQuart',
    offset: -104,
}
const Banner = () => {
    const toggleClass = () => {
        const oldClassName = document.body.className;
        const newClassName = oldClassName === 'font-bigger' ? '' : 'font-bigger'
        document.body.className = newClassName
    }
    const scrollToSection = name => {
        scroller.scrollTo(name, scrollOptions)
    }

    // const toggleFont = () => {
    //     console.log(isFontBigger);
    //     setIsFontBigger();
    //     document.body.className='font-bigger';
    // }

    return <section className="banner">
        <img className="banner__main" src="./assets/images/banner/Center.png"/>
        <img className="banner__hands" src="./assets/images/banner/Hands.png"/>
        <img className="banner__arrow"
             onClick={() => {
                 scrollToSection('description')
             }} src="./assets/images/arrow_down.png"/>
        <a className="banner__font_changer" onClick={() => {
            toggleClass()
        }}>
            <img src="./assets/images/font_changer.png" alt="change font size"/>
        </a>
    </section>
};

export default Banner;
