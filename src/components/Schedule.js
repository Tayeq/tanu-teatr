import React from 'react';
import Container from "reactstrap/lib/Container";
import {Fade, Slide} from 'react-reveal';

import ScheduleTerm from "./ScheduleTerm";


const leftCol = [
    {
        date: "29.06 \n/ godz. 12:00",
        description: "końcowy termin \nzgłaszania projektów"
    },
    {
        date: "06.07",
        description: "ogłoszenie wyników \npierwszej części"
    },
    {
        date: "od 03.08",
        description: "prezentacja nagrodzonych projektów"
    },
]

const rightCol = [
    {
        date: "8-10.07",
        description: "spotkania produkcyjne z autorami projektów zakwalifikowanych do drugiej części konkursu"
    },
    {
        date: "11.09",
        description: "ukończenie prac nad rozbudowanymi wersjami projektów"
    },
    {
        date: "od 27.09",
        description: "prezentacja rozbudowanych projektów"
    },
]

const Schedule = () => {
    return (
        <section className="schedule">
            <Container>
                <Fade>
                    <h1 className="section-title">Harmonogram</h1>
                </Fade>

                <div className="schedule__content">
                    <Slide left>
                        <div className="schedule__content-column">
                            <h2 className="schedule__column-header">część 1</h2>
                            {leftCol.map(term => <ScheduleTerm term={term} key={term.date}/>)}
                        </div>
                    </Slide>
                    <Fade>
                        <img src="./assets/images/schedule_middle.png" alt="" className="schedule__content-image"/>
                    </Fade>
                    <Slide right>
                        <div className="schedule__content-column">
                            <h2 className="schedule__column-header">część 2</h2>
                            {rightCol.map(term => <ScheduleTerm term={term} key={term.date}/>)}
                        </div>
                    </Slide>
                </div>
            </Container>

        </section>
    );
};

export default Schedule;