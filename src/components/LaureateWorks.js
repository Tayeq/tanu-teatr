import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import {Fade, Slide} from 'react-reveal';
import LaureateWork from "./LaureateWork";

const works = [
    {
        title: 'DYSONIA nowe słuchanie',
        authors: [
            'Agata Dyczko', 'Wojtek Kurek'
        ],
        links: [
            {title: 'nagranie 1', url: 'https://youtu.be/JPApMUeCqj8'},
            {title: 'nagranie 2', url: 'https://youtu.be/zaRpP4ZAHRY'},
            {title: 'tekst', url: './assets/works/dysonia2019_1.pdf'},
        ]
    },
    {
        title: 'MIŁOŚĆ TO WOLNOŚĆ W CZERWCOWY DZIEŃ',
        authors: [
            'Anna Mazurek',
            'Mateusz Górniak',
            'Konrad Kiersnowski',
            'Radosia Niezgoda',
            'Natalia Bielecka',
            'Maciej Kobiela',
            'Sandra Korzeniak',
        ],
        links: [
            {title: 'film', url: 'https://youtu.be/I_pwCzbew34'},
        ]
    },
    {
        title: '(bez tytułu)',
        authors: [
            'Katarzyna Raduszyńska',
            'Paweł Walicki',
        ],
        links: [
            {title: 'film 1', url: 'https://youtu.be/QDRCKa7jczY'},
            {title: 'film 2', url: 'https://youtu.be/lhVTxct7h8s'},
            {title: 'film 3', url: 'https://youtu.be/WcvwvOKWHNs'},
        ]
    },
    {
        title: 'Cztery słońca, dzień bez końca',
        authors: [
            'Piotr Froń',
        ],
        links: [
            {title: 'komiks', url: './assets/works/cztery_slonca.pdf'},
        ]
    },
    {
        title: 'Le Hollandais. Nawet szkice mają swoje szkice',
        authors: [
            'Przemysław Branas',
        ],
        links: [
            {title: 'prezentacja', url: './assets/works/Le_Hollandais-Przemek_Branas.pdf'},
        ]
    },
    {
        title: 'Rok ogrodnika – treatment',
        authors: [
            'Joanna Zdrada',
            'Zofia Mazurczak-Prus',
        ],
        links: [
            {title: 'prezentacja', url: './assets/works/rok_ogrodnika_jzdrada_treatment.pdf'},
        ]
    },
    {
        title: 'FRANCACHELA',
        authors: [
            'Adela Campos',
            'Julita Golińska',
            'Karina Gorzkowska',
            'Dominik Setlak',
            'Mateusz Świderski',
        ],
        links: [
            {title: 'film', url: 'https://youtu.be/CnDoITER0XE'},
        ]
    },
    {
        title: 'Agartha',
        authors: [
            'Adrianna Alksnin',
        ],
        links: [
            {title: 'tekst', url: './assets/works/Agartha-opis projektu.pdf'},
            {title: 'tekst', url: './assets/works/Listy_Profesora,_zredagowane_przez_Adriannę_Alksnin.pdf'},
        ]
    },
    {
        title: 'Czy pani wie po co pani tu przyszła',
        authors: [
            'Lucy Sosnowska',
            'Tobiasz Sebastian Berg',
            'Elżbieta Szurpicka',
        ],
        links: [
            {title: 'zdjęcie', url: './assets/works/czy_pani_wie_po_co_pani_tu_przyszła.jpg'},
            {title: 'film 1', url: 'https://youtu.be/FN_t6J-Pn3c'},
            {title: 'film 2', url: 'https://youtu.be/hlSyNuJl_8s'},
            {title: 'film 3', url: 'https://youtu.be/HY8EN10k-D8'},
            {title: 'film 4', url: 'https://youtu.be/OJO7k4mJY6o'},
        ]
    },
    {
        title: 'SANATORIUM',
        authors: [
            'Barbara Gryka',
        ],
        links: [
            {title: 'film', url: 'https://youtu.be/WSvxEP_CJDU'},
        ]
    },
];

const LaureateWorks = () => {
    return (
        <section className="laureate-works">
            <Container>
                <Fade>
                    <h1 className="section-title">Prace laureackie</h1>
                </Fade>

                <div className="laureate-works__content pt-5">
                    <Row className="mb-5">
                        <Col md="4">
                            <p className="font-bold">
                                Wyróżnione prace tworzą unikalną Antologię Nowych Utopii. Jej autorzy i autorki nadają osobisty kształt ogólnemu terminowi „utopia”, doprecyzowując go w wybranych przez siebie kontekstach. Zaprezentowane ujęcia tematu przyszłości są różnorodne, jednak wszystkie charakteryzuje potrzeba wnikliwego przyglądania się chwiejnemu światu.
                            </p>
                        </Col>
                        <Col md="7" className="d-flex align-items-center offset-md-1 p-5">
                            <img className="img-fluid" src="./assets/images/laureate_works.png"/>
                        </Col>
                    </Row>
                    <div className="laureate-works__works d-flex justify-content-between">
                        {works.map(work => <LaureateWork work={work} key={work.toString()}/>)}
                    </div>
                </div>
            </Container>
        </section>
    );
};

export default LaureateWorks;
