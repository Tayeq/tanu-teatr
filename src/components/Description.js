import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import {Fade, Roll, Slide} from 'react-reveal';

import Separator from "./Separator";

const Description = () => {
    return (
        <section className="description">
            <Separator/>
            <Fade>
                <Container>
                    <h1 className="section-title">Opis konkursu</h1>
                    <Row className="description__container">
                        <Col md="4">
                            <p className="description__part-1">Jak po pandemii będzie wyglądało życie teatralne? Na przekór uparcie powracającym obawom i frustracjom, wybiegnijmy myślą w przyszłość i skupmy się na wizjach lepszego świata. Jako Teatr Zagłębia w Sosnowcu ogłaszamy konkurs na cyfrowe dzieło artystyczne, którego tematem są marzenia, utopie, raje, światy doskonałe. Do udziału zapraszamy artystów i artystki indywidualne oraz zespoły.</p>
                        </Col>
                        <Col md="4">
                            <p  >Konkurs składa się z dwóch części.</p>
                            <p className="font-bold font-yellow">W pierwszej części jury wyłania 10 najlepszych realizacji spośród propozycji nadesłanych w formie plików cyfrowych. Autorzy i autorki każdej ze zwycięskich prac otrzymują 1500 zł brutto.</p>
                            <p className="font-bold font-yellow">W drugiej części konkursu autorzy dwóch najwyżej ocenionych prac przygotują rozbudowane wersje początkowych projektów. Grant na ten cel wynosi 10 500 zł brutto.</p>
                        </Col>
                        <Col md="4">
                            <p className="font-bold">Uczestnikom i uczestniczkom drugiej części konkursu zapewnimy wsparcie twórców i twórczyń nowych mediów w zakresie montażu filmowego, reżyserii dźwięku, animacji komputerowej oraz web designu. Uczestnicy otrzymają także możliwość korzystania z sali teatralnej, wyposażenia technicznego teatru, pomoc organizacyjną i promocyjną. Jedyne wymaganie z naszej strony to zaangażowanie w projekt trzech etatowych aktorów/aktorek Teatru Zagłębia.</p>
                            <img className="img-fluid description__image" src="./assets/images/description_image.png"/>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="d-flex justify-content-center pb-5">
                            <a href="./download/tanu-ogloszenie-2.pdf" className="admissions__button" target="_blank"
                               rel="noopener noreferrer">pobierz<br/><strong>Opis Konkursu</strong></a>
                            <a href="./download/Regulamin Teatralna Antologia Nowych Utopii.pdf"
                               className="admissions__button"
                               target="_blank" rel="noopener noreferrer">pobierz<br/><strong>Regulamin Konkursu</strong></a>
                        </Col>

                    </Row>

                </Container>
            </Fade>
        </section>
    );
};

export default Description;
