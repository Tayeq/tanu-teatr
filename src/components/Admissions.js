import React from 'react';
import Container from "reactstrap/lib/Container";
import {Fade} from 'react-reveal';

const Admissions = () => {
    return (
        <section className="admissions">
            <Container>
                <Fade>
                <h1 className="section-title">Zgłoszenia</h1>
                </Fade>
                <Fade cascade>
                    <div className="admissions__content">
                        <a href="./download/tanu-ogloszenie-2.pdf" className="admissions__button" target="_blank"
                           rel="noopener noreferrer">pobierz<br/><strong>Opis Konkursu</strong></a>
                        <p>Prosimy o wysyłanie zgłoszeń drogą mailową na adres<br/>
                            <a href="mailto:tanu@teatrzaglebia.pl"><strong>tanu@teatrzaglebia.pl</strong></a>.</p>
                        <a href="./download/Regulamin Teatralna Antologia Nowych Utopii.pdf"
                           className="admissions__button"
                           target="_blank" rel="noopener noreferrer">pobierz<br/><strong>Regulamin Konkursu</strong></a>
                        <a href="./download/TANU-formularz_zgłoszeniowy.doc" className="admissions__button"
                           target="_blank"
                           rel="noopener noreferrer">pobierz<br/><strong>Formularz zgłoszeniowy</strong></a>
                        <img className="admissions__image" src="./assets/images/admissions_bottom_image.png" alt=""/>
                    </div>
                </Fade>
            </Container>
        </section>
    );
};

export default Admissions;