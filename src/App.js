import React from 'react'
import {Root, addPrefetchExcludes} from 'react-static'

import './styles/app.scss'
import MainNavbar from "./components/MainNavbar";
import Description from "./components/Description";
import CompetitionWorks from "./components/CompetitionWorks";
import LaureateWorks from "./components/LaureateWorks";
import Banner from "./components/Banner";
import Schedule from "./components/Schedule";
import Admissions from "./components/Admissions";
import Footer from "./components/Footer";

addPrefetchExcludes(['dynamic'])

function App() {

    if(typeof document !== 'undefined'){
        document.title = "TANU";
    }
    return (
        <Root>
            <MainNavbar/>
            <main>
                <Banner/>
                <Description/>
                <CompetitionWorks/>
                <LaureateWorks/>
                <Schedule/>
            </main>
            <Footer/>
        </Root>
    )
}

export default App
